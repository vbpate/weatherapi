Feature: Weather forecast for Sydney

@tag1
  Scenario: Temprature predicton for above twenty degrees
    Given I call the weather API for "Sydney"
    When I do the get call for weather API
    Then number of days should be return where the temprature is predicted to be above twenty degrees for next five days
    And number of days predeicted to be sunny in the same time period