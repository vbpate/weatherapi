package utils;

import java.io.IOException;
import java.util.Properties;
import java.util.logging.Logger;

public class LoadDataHub {

	static DataHub dataHub = null;
	private static final Logger LOGGER = Logger.getLogger(LoadDataHub.class.getName());

	private static Properties getProperties() throws IOException {
		Properties prop = new Properties();
		prop.load(LoadDataHub.class.getClassLoader().getResourceAsStream("constants.properties"));
		return prop;
	}

	public static DataHub loadDetails() {
		if (null == dataHub) {
			try {
				Properties prop = getProperties();
				dataHub = new DataHub();
				dataHub.setHostURL(prop.getProperty("hostURL"));
				dataHub.setApiKey(prop.getProperty("apiKey"));
				dataHub.setUnits(prop.getProperty("units"));
			} catch (IOException e) {
				LOGGER.info("File not found, Please check parameters");
			}
		}
		return dataHub;
	}
}
