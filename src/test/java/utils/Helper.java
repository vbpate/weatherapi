package utils;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

public class Helper {

	public Map<String, String> loadTemperatureData(String response) {
		return fetchDataforField(response, "temperature");
	}

	public Map<String, String> loadWeatherData(String response) {
		return fetchDataforField(response, "weather");
	}

	private Map<String, String> fetchDataforField(String response, String fieldName) {
		Map<String, String> dataMap = new HashMap<String, String>();
		JSONObject jsonObject = new JSONObject(response.toString().trim());
		JSONArray jsonarray = new JSONArray(jsonObject.get("list").toString());
		for (Object keyStr : jsonarray) {
			dataMap = getEachDayFieldData(dataMap, new JSONObject(keyStr.toString().trim()), fieldName);
		}
		return dataMap;
	}

	private Map<String, String> getEachDayFieldData(Map<String, String> dataMap, JSONObject jsonObj,
			String dataProperty) {
		for (String keyStr : jsonObj.keySet()) {
			{
				Object keyvalue = jsonObj.get(keyStr);
				if (keyStr.equalsIgnoreCase("dt_txt")) {
					String date = keyvalue.toString().split(" ")[0];
					switch (dataProperty) {
					case "temperature":
						String tempValue = getJsonKeyValue(new JSONObject(jsonObj.get("main").toString().trim()),
								"temp");
						if (dataMap.containsKey(date)) {
							if (Float.parseFloat(dataMap.get(date).toString()) < Float.parseFloat(tempValue))
								dataMap.put(keyvalue.toString().split(" ")[0], tempValue);
						} else {
							dataMap.put(keyvalue.toString().split(" ")[0], tempValue);
						}
						break;
					case "weather":
						String weatherValue = getJsonKeyValue(new JSONObject(
								jsonObj.get("weather").toString().replaceAll("]", "").replaceAll("\\[", "").trim()),
								"main");
						if (dataMap.containsKey(date)) {
							if (!dataMap.get(date).toString().equalsIgnoreCase("clear"))
								dataMap.put(keyvalue.toString().split(" ")[0], weatherValue);
						} else {
							dataMap.put(keyvalue.toString().split(" ")[0], weatherValue);
						}
						break;
					}
				}
			}
		}
		return dataMap;
	}

	public static String getJsonKeyValue(JSONObject jsonObj, String keyName) {
		for (String keyStr : jsonObj.keySet()) {
			{
				if (keyStr.equalsIgnoreCase(keyName)) {
					return jsonObj.get(keyStr).toString();
				}
			}
		}
		return "";
	}
}
