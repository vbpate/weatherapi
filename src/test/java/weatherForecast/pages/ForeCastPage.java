package weatherForecast.pages;

import java.io.IOException;
import java.util.logging.Logger;

import actions.HttpOperation;
import net.serenitybdd.core.pages.PageObject;
import utils.LoadDataHub;

public class ForeCastPage extends PageObject {

	private static final Logger LOGGER = Logger.getLogger(ForeCastPage.class.getName());

	public String getWeatherDataforCity(String city) {
		String response = "";
		HttpOperation httpOperation = new HttpOperation();
		try {
			String requestURL = LoadDataHub.loadDetails().getHostURL() + "?q=" + city + "&units="
					+ LoadDataHub.loadDetails().getUnits() + "&appid=" + LoadDataHub.loadDetails().getApiKey();
			response = httpOperation.sendGET(requestURL, LoadDataHub.loadDetails().getUser_agent());
		} catch (IOException e) {
			LOGGER.info("Please check all the paramters" + e);
		}
		return response;
	}
}
