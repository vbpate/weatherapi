package weatherForecast.glue;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;
import weatherForecast.steps.ForeCastSteps;

public class ForeCastGlue {

	@Steps
	ForeCastSteps foreCastSteps;

	@Given("^I call the weather API for \"([^\"]*)\"$")
	public void i_call_the_weather_API_for(String city) {
		foreCastSteps.setCity(city);
	}

	@When("^I do the get call for weather API$")
	public void i_do_the_get_call_for_weather_API() {
		foreCastSteps.getWeatherData();
	}

	@Then("^number of days should be return where the temprature is predicted to be above twenty degrees for next five days$")
	public void number_of_days_should_be_return_where_the_temprature_is_predicted_to_be_above_twenty_degrees_for_next_five_days() {
		foreCastSteps.processTemperatureData();
	}

	@Then("^number of days predeicted to be sunny in the same time period$")
	public void number_of_days_predeicted_to_be_sunny_in_the_same_time_period() {
		foreCastSteps.processWeatherData();
	}
}
