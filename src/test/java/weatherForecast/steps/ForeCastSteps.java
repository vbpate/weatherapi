package weatherForecast.steps;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;
import java.util.Map;
import java.util.logging.Logger;

import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;
import utils.Helper;
import weatherForecast.pages.ForeCastPage;

public class ForeCastSteps extends ScenarioSteps {

	private static final Logger LOGGER = Logger.getLogger(ForeCastSteps.class.getName());
	private String city = "";
	private String response = "";

	ForeCastPage foreCastPage = new ForeCastPage();

	@Step
	public void setCity(String city) {
		this.city = city;
	}

	@Step
	public void getWeatherData() {
		ForeCastPage foreCastPage = new ForeCastPage();
		response = foreCastPage.getWeatherDataforCity(city);
	}

	@Step
	public void processTemperatureData() {
		Helper dataHelper = new Helper();
		Map<String, String> dayTempMap = dataHelper.loadTemperatureData(response);
		dayTempMap.remove(DateTimeFormatter.ofPattern("yyyy-MM-dd", Locale.ENGLISH).format(LocalDateTime.now()));
		int i = 0;
		for (String key : dayTempMap.keySet()) {
			if (Float.parseFloat(dayTempMap.get(key).toString()) > 20) {
				i++;
			}
		}
		LOGGER.info("Number of days temperature is predicated to be above 20 degrees in the next 5 days are: " + i);
	}

	@Step
	public void processWeatherData() {
		Helper dataHelper = new Helper();
		Map<String, String> dayTempMap = dataHelper.loadWeatherData(response);
		dayTempMap.remove(DateTimeFormatter.ofPattern("yyyy-MM-dd", Locale.ENGLISH).format(LocalDateTime.now()));
		int i = 0;
		for (String key : dayTempMap.keySet()) {
			if (dayTempMap.get(key).toString().equalsIgnoreCase("clear")) {
				i++;
			}
		}
		LOGGER.info("Number of days weather is predicted to be sunny in the next 5 days are: " + i);
	}

}
