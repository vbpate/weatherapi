## NB:  Please add API key in constants.properties file which is located under src/test/resources folder

# Framework structure
* Serenity bdd framework is used to implement desired outcome
* Gradle is used for build in version tool to add all the dependencies
# Please follow below steps to run the automated test execution 
 * open the command prompt
 * run the command : gradlew build
 * run the command : gradlew --rerun-tasks --info
# Report
* Navigate to below folder to see serenity report:
   * target/site/serenity/index.html
        * This has a human readable pie chart showing the result of the test run
        * One can drill down into individual tests and steps of those tests